/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.twoactivities;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static java.lang.Thread.sleep;
import static java.util.regex.Pattern.matches;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    /* WeB
    1.what
     */


    public ActivityTestRule activityRule =  new ActivityTestRule<>(MainActivity.class);

    //TC1 TEST that when you push it send to next screen
    //TC2Test then when you type nonsense onto box text appears on page 2



     @Test
     public void testGoingToNextPage(){
     onView(withId(R.id.button_main)).perform(click());

      sleep(3000);

         onView(withId(R.id.text_header)).check(matches(isDislplayed()));

         sleep(1000);

         //-- go back to previous page
                 //--  find reply button and clcik on it
         onView(withId(R.id.button_second)).perform(click());

         sleep(1000);


     }

     @Test
     public void testInputBox(){

         //1.find the text box

         onView(withId(R.id.editText_main)).perform(typeText("HELLO ARYA"));
         //2.type some nonsense (sendKeys() in selenium)

         onView(withId(R.id.button_second)).perform(click());



         //check the words inside the label//
         onView(withId(R.id.text_message)).check(matches(withText(exceptedOutput)));


         sleep(1000);




     }







}
